package indi.xulala.option;

import indi.xulala.data.LegendData;
import indi.xulala.enums.Orient;
import indi.xulala.enums.type.LegendType;
import indi.xulala.enums.X;
import indi.xulala.enums.Y;
import indi.xulala.style.TextStyle;

import java.util.List;

/**
 * @Author：xulei
 * @Description：
 * 图例组件。
 * 图例组件展现了不同系列的标记(symbol)，颜色和名字。可以通过点击图例控制哪些系列不显示。
 * ECharts 3 中单个 echarts 实例中可以存在多个图例组件，会方便多个图例的布局。
 * 当图例数量过多时，可以使用 滚动图例（垂直） 或 滚动图例（水平），参见：legend.type
 * @Date：2018/4/3 0003 13:34
 */
public class Legend {
    /**
     * 图例的类型。可选值
     * 'plain'：普通图例。缺省就是普通图例。
     * 'scroll'：可滚动翻页的图例。当图例数量较多时可以使用。
     * * */
    private LegendType type= LegendType.plain;
    /**
     * [ default: true ]
     * * */
    private boolean show=true;
    /**
     * 所有图形的 zlevel 值。
     * zlevel用于 Canvas 分层，不同zlevel值的图形会放置在不同的 Canvas 中，Canvas 分层是一种常见的优化手段。
     * 我们可以把一些图形变化频繁（例如有动画）的组件设置成一个单独的zlevel。
     * 需要注意的是过多的 Canvas 会引起内存开销的增大，在手机端上需要谨慎使用以防崩溃。
     * zlevel 大的 Canvas 会放在 zlevel 小的 Canvas 的上面。
     *[ default: 0 ]
     * * */
    private Number zlevel=0;
    /**
     * 组件的所有图形的z值。控制图形的前后顺序。
     * z值小的图形会被z值大的图形覆盖。
     * z相比zlevel优先级更低，而且不会创建新的 Canvas。
     *[ default: 2 ]
     * * */
    private Number z=2;
    /**
     * 图例组件离容器左侧的距离。
     * left 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
     * 如果 left 的值为'left', 'center', 'right'，组件会根据相应的位置自动对齐。
     * * */
    private Object left= X.auto;
    /**
     * 图例组件离容器上侧的距离。
     * top 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'。
     * 如果 top 的值为'top', 'middle', 'bottom'，组件会根据相应的位置自动对齐。
     * * */
    private Object top= Y.auto;
    /**
     * 图例组件离容器右侧的距离。
     * right 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
     * 默认自适应。
     * * */
    private Object right= X.auto;
    /**
     * 图例组件离容器下侧的距离。
     * bottom 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
     * 默认自适应。
     * * */
    private Object bottom= Y.auto;
    /**
     * 图例组件的高度。默认自适应。
     * * */
    private Object width= X.auto;
    /**
     * 图例列表的布局朝向。
     * 可选：'horizontal'
     * 'vertical'
     * [ default: 'horizontal' ]
     * * */
    private Orient orient= Orient.horizontal;
    /**
     * 图例标记和文本的对齐。默认自动，根据组件的位置和 orient 决定，当组件的 left 值为 'right' 以及纵向布局（orient 为 'vertical'）的时候为右对齐，及为 'right'。
     * [ default: 'auto' ]
     * * */
    private X align= X.auto;
    /**
     * 图例内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距。
     * 使用示例：
     * // 设置内边距为 5
     * padding: 5
     * // 设置上下的内边距为 5，左右的内边距为 10
     * padding: [5, 10]
     * // 分别设置四个方向的内边距
     * padding: [
     * 5,  // 上
     * 10, // 右
     * 5,  // 下
     * 10, // 左
     * ]
     * [ default: 5 ]
     * * */
    private Number padding= 5;
    /**
     * 图例每项之间的间隔。横向布局时为水平间隔，纵向布局时为纵向间隔。
     * [ default: 10 ]
     * * */
    private Number itemGap= 10;
    /**
     * 图例标记的图形宽度。
     * [ default: 25 ]
     * * */
    private Number itemWidth= 25;
    /**
     * 图例标记的图形宽度。
     * [ default: 14 ]
     * * */
    private Number itemHeight= 14;
    /**
     * 用来格式化图例文本，支持字符串模板和回调函数两种形式。
     * 示例：
     * // 使用字符串模板，模板变量为图例名称 {name}
     * formatter: 'Legend {name}'
     * // 使用回调函数
     * formatter: function (name) {
     * return 'Legend ' + name;
     * }
     * * */
    private Object formatter;
    /**
     * 图例选择的模式，控制是否可以通过点击图例改变系列的显示状态。
     * 默认开启图例选择，可以设成 false 关闭。
     * 除此之外也可以设成 'single' 或者 'multiple' 使用单选或者多选模式。
     * [ default: true ]
     * * */
    private Object selectedMode=true;
    /**
     * 图例关闭时的颜色。
     * * */
    private String inactiveColor="#ccc";
    /**
     * 图例选中状态表。
     * 示例：
     * selected: {
     * // 选中'系列1'
     * '系列1': true,
     * // 不选中'系列2'
     * '系列2': false
     * }
     * * */
    private Object selected;
    /**
     * 图例的公用文本样式。
     * * */
    private TextStyle textStyle;
    /**
     * 图例的 tooltip 配置，配置项同 tooltip。
     * 默认不显示，可以在 legend 文字很多的时候对文字做裁剪并且开启 tooltip，如下示例：
     * legend: {
     * formatter: function (name) {
     * return echarts.format.truncateText(name, 40, '14px Microsoft Yahei', '…');
     * },
     * tooltip: {
     * show: true
     * }
     * }
     * * */
    private Object tooltip;
    /**
     * 图例的数据数组。
     * * */
    private List<LegendData> data;
    /**
     * 图例背景色，默认透明。
     * [ default: 'transparent' ]
     * * */
    private String backgroundColor="transparent";
    /**
     * 图例的边框颜色。支持的颜色格式同
     * [ default: '#ccc' ]
     * * */
    private String borderColor="#ccc";
    /**
     * 图例的边框线宽。
     * [ default: 1 ]
     * * */
    private Number borderWidth=1;
    /**
     * 圆角半径，单位px，支持传入数组分别指定 4 个圆角半径。 如:
     * borderRadius: 5, // 统一设置四个角的圆角大小
     * borderRadius: [5, 5, 0, 0] //（顺时针左上，右上，右下，左下）
     * [ default: 0 ]
     * * */
    private Object borderRadius=0;
    /**
     * 图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果。
     * 示例：
     * {
     * shadowColor: 'rgba(0, 0, 0, 0.5)',
     * shadowBlur: 10
     * }
     * 注意：此配置项生效的前提是，设置了 show: true 以及值不为 tranparent 的背景色 backgroundColor。
     * * */
    private Number shadowBlur;
    /**
     * 阴影颜色。支持的格式同color。
     * 注意：此配置项生效的前提是，设置了 show: true。
     * */
    private String shadowColor;
    /**
     * 阴影水平方向上的偏移距离。
     * 注意：此配置项生效的前提是，设置了 show: true。
     * [ default: 0 ]
     * */
    private Number shadowOffsetX=0;
    /**
     * 阴影垂直方向上的偏移距离。
     * 注意：此配置项生效的前提是，设置了 show: true。
     * [ default: 0 ]
     * */
    private Number shadowOffsetY=0;
    /**
     * legend.type 为 'scroll' 时有效。
     * 图例控制块中，按钮和页信息之间的间隔。
     * 参见 滚动图例（垂直） 或 滚动图例（水平）。
     * [ default: 0 ]
     * */
    private Number scrollDataIndex=0;

    /**
     * legend.type 为 'scroll' 时有效。
     * 图例当前最左上显示项的 dataIndex。
     * setOption 时指定此项的话，可决定当前图例滚动到哪里。
     * 但是，如果仅仅想改变图例翻页，一般并不调用 setOption（因为这太重量了），仅仅使用 action legendScroll 即可。
     * [ default: 0 ]
     * */
    private Number pageButtonItemGap=5;

    /**
     * legend.type 为 'scroll' 时有效。
     * 图例控制块和图例项之间的间隔。
     * 参见 滚动图例（垂直） 或 滚动图例（水平）。
     * [ default: 0 ]
     * */
    private Number pageButtonGap;

    /**
     * legend.type 为 'scroll' 时有效。
     * ]图例控制块中，页信息的显示格式。默认为 '{current}/{total}'，其中 {current} 是当前页号（从 1 开始计数），{total} 是总页数。
     * 如果 pageFormatter 使用函数，须返回字符串，参数为：
     * {
     * current: number
     * total: number
     * }
     * [ default: '{current}/{total}' ]
     * */
    private Object pageFormatter = "{current}/{total}";

    /**
     * legend.type 为 'scroll' 时有效。
     * 图例控制块的图标。
     * */
    private PageIcons pageIcons;

    /**
     * legend.type 为 'scroll' 时有效。
     * 翻页按钮的颜色。
     * [ default: '#2f4554' ]
     * */
    private String pageIconColor="#2f4554";

    /**
     * legend.type 为 'scroll' 时有效。
     * 翻页按钮不激活时（即翻页到头时）的颜色。
     * [ default: '#aaa' ]
     * */
    private String pageIconInactiveColor="#aaa";

    /**
     * legend.type 为 'scroll' 时有效。
     * 翻页按钮的大小。可以是数字，也可以是数组，如 [10, 3]，表示 [宽，高]。
     * [ default: 15 ]
     * */
    private Object pageIconSize=15;
    /**
     * 图例页信息的文字样式。
     * * */
    private TextStyle pageTextStyle;
    /**
     * 图例翻页是否使用动画。
     * * */
    private boolean animation;
    /**
     * 图例翻页时的动画时长。
     * * */
    private Number animationDurationUpdate=800;


    public Legend() {
    }

    public LegendType getType() {
        return type;
    }

    public Legend setType(LegendType type) {
        this.type = type;
        return this;
    }

    public boolean isShow() {
        return show;
    }

    public Legend setShow(boolean show) {
        this.show = show;
        return this;
    }

    public Number getZlevel() {
        return zlevel;
    }

    public Legend setZlevel(Number zlevel) {
        this.zlevel = zlevel;return this;
    }

    public Number getZ() {
        return z;
    }

    public Legend setZ(Number z) {
        this.z = z;return this;
    }

    public Object getLeft() {
        return left;
    }

    public Legend setLeft(Object left) {
        this.left = left;return this;
    }

    public Object getTop() {
        return top;
    }

    public Legend setTop(Object top) {
        this.top = top;return this;
    }

    public Object getRight() {
        return right;
    }

    public Legend setRight(Object right) {
        this.right = right;return this;
    }

    public Object getBottom() {
        return bottom;
    }

    public Legend setBottom(Object bottom) {
        this.bottom = bottom;return this;
    }

    public Object getWidth() {
        return width;
    }

    public Legend setWidth(Object width) {
        this.width = width;return this;
    }

    public Orient getOrient() {
        return orient;
    }

    public Legend setOrient(Orient orient) {
        this.orient = orient;return this;
    }

    public X getAlign() {
        return align;
    }

    public Legend setAlign(X align) {
        this.align = align;return this;
    }

    public Number getPadding() {
        return padding;
    }

    public Legend setPadding(Number padding) {
        this.padding = padding;return this;
    }

    public Number getItemGap() {
        return itemGap;
    }

    public Legend setItemGap(Number itemGap) {
        this.itemGap = itemGap;return this;
    }

    public Number getItemWidth() {
        return itemWidth;
    }

    public Legend setItemWidth(Number itemWidth) {
        this.itemWidth = itemWidth;return this;
    }

    public Number getItemHeight() {
        return itemHeight;
    }

    public Legend setItemHeight(Number itemHeight) {
        this.itemHeight = itemHeight;return this;
    }

    public Object getFormatter() {
        return formatter;
    }

    public Legend setFormatter(Object formatter) {
        this.formatter = formatter;return this;
    }

    public Object getSelectedMode() {
        return selectedMode;
    }

    public Legend setSelectedMode(Object selectedMode) {
        this.selectedMode = selectedMode;return this;
    }

    public String getInactiveColor() {
        return inactiveColor;
    }

    public Legend setInactiveColor(String inactiveColor) {
        this.inactiveColor = inactiveColor;return this;
    }

    public Object getSelected() {
        return selected;
    }

    public Legend setSelected(Object selected) {
        this.selected = selected;return this;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public Legend setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;return this;
    }

    public Object getTooltip() {
        return tooltip;
    }

    public Legend setTooltip(Object tooltip) {
        this.tooltip = tooltip;return this;
    }

    public List<LegendData> getData() {
        return data;
    }

    public Legend setData(List<LegendData> data) {
        this.data = data;return this;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public Legend setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;return this;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public Legend setBorderColor(String borderColor) {
        this.borderColor = borderColor;return this;
    }

    public Number getBorderWidth() {
        return borderWidth;
    }

    public Legend setBorderWidth(Number borderWidth) {
        this.borderWidth = borderWidth;return this;
    }

    public Object getBorderRadius() {
        return borderRadius;
    }

    public Legend setBorderRadius(Object borderRadius) {
        this.borderRadius = borderRadius;return this;
    }

    public Number getShadowBlur() {
        return shadowBlur;
    }

    public Legend setShadowBlur(Number shadowBlur) {
        this.shadowBlur = shadowBlur;return this;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public Legend setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;return this;
    }

    public Number getShadowOffsetX() {
        return shadowOffsetX;
    }

    public Legend setShadowOffsetX(Number shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;return this;
    }

    public Number getShadowOffsetY() {
        return shadowOffsetY;
    }

    public Legend setShadowOffsetY(Number shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;return this;
    }

    public Number getScrollDataIndex() {
        return scrollDataIndex;
    }

    public Legend setScrollDataIndex(Number scrollDataIndex) {
        this.scrollDataIndex = scrollDataIndex;return this;
    }

    public Number getPageButtonItemGap() {
        return pageButtonItemGap;
    }

    public Legend setPageButtonItemGap(Number pageButtonItemGap) {
        this.pageButtonItemGap = pageButtonItemGap;return this;
    }

    public Number getPageButtonGap() {
        return pageButtonGap;
    }

    public Legend setPageButtonGap(Number pageButtonGap) {
        this.pageButtonGap = pageButtonGap;return this;
    }

    public Object getPageFormatter() {
        return pageFormatter;
    }

    public Legend setPageFormatter(Object pageFormatter) {
        this.pageFormatter = pageFormatter;return this;
    }

    public PageIcons getPageIcons() {
        return pageIcons;
    }

    public Legend setPageIcons(PageIcons pageIcons) {
        this.pageIcons = pageIcons;return this;
    }

    public String getPageIconColor() {
        return pageIconColor;
    }

    public Legend setPageIconColor(String pageIconColor) {
        this.pageIconColor = pageIconColor;return this;
    }

    public String getPageIconInactiveColor() {
        return pageIconInactiveColor;
    }

    public Legend setPageIconInactiveColor(String pageIconInactiveColor) {
        this.pageIconInactiveColor = pageIconInactiveColor;return this;
    }

    public Object getPageIconSize() {
        return pageIconSize;
    }

    public Legend setPageIconSize(Object pageIconSize) {
        this.pageIconSize = pageIconSize;return this;
    }

    public TextStyle getPageTextStyle() {
        return pageTextStyle;
    }

    public Legend setPageTextStyle(TextStyle pageTextStyle) {
        this.pageTextStyle = pageTextStyle;return this;
    }

    public boolean isAnimation() {
        return animation;
    }

    public Legend setAnimation(boolean animation) {
        this.animation = animation;return this;
    }

    public Number getAnimationDurationUpdate() {
        return animationDurationUpdate;
    }

    public Legend setAnimationDurationUpdate(Number animationDurationUpdate) {
        this.animationDurationUpdate = animationDurationUpdate;return this;
    }

    class PageIcons{
        /**
         *legend.orient 为 'horizontal' 时的翻页按钮图标。
         * 是一个数组，表示 [previous page button, next page button]。默认值为 ['M0,0L12,-10L12,10z', 'M0,0L-12,-10L-12,10z']，。
         * 数组中每项，
         * 可以通过 'image://url' 设置为图片，其中 url 为图片的链接，或者 dataURI。
         * 可以通过 'path://' 将图标设置为任意的矢量路径。这种方式相比于使用图片的方式，不用担心因为缩放而产生锯齿或模糊，而且可以设置为任意颜色。路径图形会自适应调整为合适的大小。路径的格式参见 SVG PathData。可以从 Adobe Illustrator 等工具编辑导出。
         * */
        private List horizontal;
        /**
         *legend.orient 为 'vertical' 时的翻页按钮图标。
         * 是一个数组，表示 [previous page button, next page button]。默认值为 ['M0,0L12,-10L12,10z', 'M0,0L-12,-10L-12,10z']，。
         * 数组中每项，
         * 可以通过 'image://url' 设置为图片，其中 url 为图片的链接，或者 dataURI。
         * 可以通过 'path://' 将图标设置为任意的矢量路径。这种方式相比于使用图片的方式，不用担心因为缩放而产生锯齿或模糊，而且可以设置为任意颜色。路径图形会自适应调整为合适的大小。路径的格式参见 SVG PathData。可以从 Adobe Illustrator 等工具编辑导出。
         * */
        private List vertical;

        public PageIcons() {
        }

        public List getHorizontal() {
            return horizontal;
        }

        public PageIcons setHorizontal(List horizontal) {
            this.horizontal = horizontal;return this;
        }

        public List getVertical() {
            return vertical;
        }

        public PageIcons setVertical(List vertical) {
            this.vertical = vertical;return this;
        }
    }
}
