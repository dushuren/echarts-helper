package indi.xulala.option;

import indi.xulala.enums.Target;
import indi.xulala.enums.X;
import indi.xulala.enums.Y;
import indi.xulala.style.TextStyle;

/**
 * @Author：xulei
 * @Description：
 * 标题组件，包含主标题和副标题。
 * 在 ECharts 2.x 中单个 ECharts 实例最多只能拥有一个标题组件。
 * 但是在 ECharts 3 中可以存在任意多个标题组件，这在需要标题进行排版，或者单个实例中的多个图表都需要标题时会比较有用。
 * @Date：2018/4/2 0002 17:00
 */
public class Title {
    /**
     * 是否显示标题组件。
     * [ default: true ]
     * */
    private boolean show=true;
    /**
     * 主标题文本，支持使用 \n 换行。
     * [ default: '' ]
     * */
    private String text="";
    /**
     * 主标题文本超链接。
     * [ default: '' ]
     * */
    private String link="";
    /**
     * 指定窗口打开主标题超链接。
     * [ default: 'blank' ]
     * */
    private Target target= Target.blank;
    /**
     * 标题文本样式。
     * */
    private TextStyle textStyle;
    /**
     * 副标题文本，支持使用 \n 换行。
     * [ default: '' ]
     * */
    private String subtext="";
    /**
     * 副标题文本超链接。
     * [ default: '' ]
     * */
    private String sublink="";
    /**
     * 指定窗口打开副标题超链接
     * [ default: 'blank' ]
     * */
    private Target subtarget= Target.blank;
    /**
     * 副标题文本样式。
     * */
    private TextStyle subtextStyle;
    /**
     * 标题内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距。[ default: 5 ]
     * 使用示例：
     * // 设置内边距为 5
     * padding: 5
     * // 设置上下的内边距为 5，左右的内边距为 10
     * padding: [5, 10]
     * // 分别设置四个方向的内边距
     * padding: [
     * 5,  // 上
     * 10, // 右
     * 5,  // 下
     * 10, // 左
     * ]
     * */
    private Number padding=5;
    /**
     * 主副标题之间的间距。
     * [ default: 10 ]
     * */
    private Number itemGap=10;
    /**
     * 所有图形的 zlevel 值。[ default: 0 ]
     * zlevel用于 Canvas 分层，不同zlevel值的图形会放置在不同的 Canvas 中，Canvas 分层是一种常见的优化手段。
     * 我们可以把一些图形变化频繁（例如有动画）的组件设置成一个单独的zlevel。
     * 需要注意的是过多的 Canvas 会引起内存开销的增大，在手机端上需要谨慎使用以防崩溃。
     * zlevel 大的 Canvas 会放在 zlevel 小的 Canvas 的上面。
     * */
    private Number zlevel=0;
    /**
     * 组件的所有图形的z值。控制图形的前后顺序。z值小的图形会被z值大的图形覆盖。
     * z相比zlevel优先级更低，而且不会创建新的 Canvas。
     * [ default: 2 ]
     * */
    private Number z=2;
    /**
     * grid 组件离容器左侧的距离。
     * left 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
     * 如果 left 的值为'left', 'center', 'right'，组件会根据相应的位置自动对齐。
     * */
    private Object left= X.auto;
    /**
     * grid 组件离容器上侧的距离。
     * top 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'。
     * 如果 top 的值为'top', 'middle', 'bottom'，组件会根据相应的位置自动对齐。
     * */
    private Object top= Y.auto;
    /**
     * grid 组件离容器右侧的距离。
     * right 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
     * 默认自适应。
     * */
    private Object right= X.auto;
    /**
     * grid 组件离容器下侧的距离。
     * bottom 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
     * 默认自适应。
     * */
    private Object bottom= Y.auto;
    /**
     * 标题背景色，默认透明。
     * 颜色可以使用 RGB 表示，比如 'rgb(128, 128, 128)' ，如果想要加上 alpha 通道，可以使用 RGBA，比如 'rgba(128, 128, 128, 0.5)'，也可以使用十六进制格式，比如 '#ccc'
     * [ default: 'transparent' ]
     * */
    private String backgroundColor="transparent";
    /**
     * 标题的边框颜色。支持的颜色格式同 backgroundColor。
     * [ default: '#ccc' ]
     * */
    private String borderColor="#ccc";
    /**
     * 标题的边框线宽。
     * [ default: 0 ]
     * */
    private Number borderWidth=0;
    /**
     * 圆角半径，单位px，支持传入数组分别指定 4 个圆角半径。 如:
     * borderRadius: 5, // 统一设置四个角的圆角大小
     * borderRadius: [5, 5, 0, 0] //（顺时针左上，右上，右下，左下）
     * [ default: 0 ]
     * */
    private Number borderRadius=0;
    /**
     * 图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果。
     * 示例：
     * {
     * shadowColor: 'rgba(0, 0, 0, 0.5)',
     * shadowBlur: 10
     * }
     * 注意：此配置项生效的前提是，设置了 show: true 以及值不为 tranparent 的背景色 backgroundColor。
     * */
    private Number shadowBlur=0;
    /**
     * 阴影颜色。支持的格式同color。
     * 注意：此配置项生效的前提是，设置了 show: true。
     * */
    private String shadowColor;
    /**
     * 阴影水平方向上的偏移距离。
     * 注意：此配置项生效的前提是，设置了 show: true。
     * */
    private Number shadowOffsetX=0;
    /**
     * 阴影水平方向上的偏移距离。
     * 注意：此配置项生效的前提是，设置了 show: true。
     * */
    private Number shadowOffsetY=0;

    public boolean isShow() {
        return show;
    }

    public Title setShow(boolean show) {
        this.show = show;
        return this;
    }

    public String getText() {
        return text;
    }

    public Title setText(String text) {
        this.text = text;
        return this;
    }

    public String getLink() {
        return link;
    }

    public Title setLink(String link) {
        this.link = link;
        return this;
    }

    public Target getTarget() {
        return target;
    }

    public Title setTarget(Target target) {
        this.target = target;
        return this;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public Title setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        return this;
    }

    public String getSubtext() {
        return subtext;
    }

    public Title setSubtext(String subtext) {
        this.subtext = subtext;
        return this;
    }

    public String getSublink() {
        return sublink;
    }

    public Title setSublink(String sublink) {
        this.sublink = sublink;
        return this;
    }

    public Target getSubtarget() {
        return subtarget;
    }

    public Title setSubtarget(Target subtarget) {
        this.subtarget = subtarget;
        return this;
    }

    public TextStyle getSubtextStyle() {
        return subtextStyle;
    }

    public Title setSubtextStyle(TextStyle subtextStyle) {
        this.subtextStyle = subtextStyle;
        return this;
    }

    public Number getPadding() {
        return padding;
    }

    public Title setPadding(Number padding) {
        this.padding = padding;
        return this;
    }

    public Number getItemGap() {
        return itemGap;
    }

    public Title setItemGap(Number itemGap) {
        this.itemGap = itemGap;
        return this;
    }

    public Number getZlevel() {
        return zlevel;
    }

    public Title setZlevel(Number zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    public Number getZ() {
        return z;
    }

    public Title setZ(Number z) {
        this.z = z;
        return this;
    }

    public Object getLeft() {
        return left;
    }

    public Title setLeft(Object left) {
        this.left = left;
        return this;
    }

    public Object getTop() {
        return top;
    }

    public Title setTop(Object top) {
        this.top = top;
        return this;
    }

    public Object getRight() {
        return right;
    }

    public Title setRight(Object right) {
        this.right = right;
        return this;
    }

    public Object getBottom() {
        return bottom;
    }

    public Title setBottom(Object bottom) {
        this.bottom = bottom;
        return this;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public Title setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public Title setBorderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Number getBorderWidth() {
        return borderWidth;
    }

    public Title setBorderWidth(Number borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Number getBorderRadius() {
        return borderRadius;
    }

    public Title setBorderRadius(Number borderRadius) {
        this.borderRadius = borderRadius;
        return this;
    }

    public Number getShadowBlur() {
        return shadowBlur;
    }

    public Title setShadowBlur(Number shadowBlur) {
        this.shadowBlur = shadowBlur;
        return this;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public Title setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
        return this;
    }

    public Number getShadowOffsetX() {
        return shadowOffsetX;
    }

    public Title setShadowOffsetX(Number shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
        return this;
    }

    public Number getShadowOffsetY() {
        return shadowOffsetY;
    }

    public Title setShadowOffsetY(Number shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
        return this;
    }
}
