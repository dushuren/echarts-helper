package indi.xulala.style;

import indi.xulala.enums.X;
import indi.xulala.enums.FontStyle;
import indi.xulala.enums.FontWeight;
import indi.xulala.enums.Y;

/**
 * @Author：xulei
 * @Description：文本样式
 * @Date：2018/4/3 0003 09:28
 */
public class TextStyle {
    /**
     *主标题文字的颜色。[ default: '#333' ]
     * */
    private String color="#333";
    /**
     *主标题文字字体的风格。[ default: 'normal' ]
     * */
    private FontStyle fontStyle= FontStyle.normal;
    /**
     *主标题文字字体的粗细。[ default: 'normal' ]
     * */
    private Object fontWeight= FontWeight.normal;
    /**
     *主标题文字的字体系列。[ default: 'sans-serif' ]
     * */
    private String fontFamily= "sans-serif";
    /**
     *主标题文字的字体大小。[ default: 18 ]
     * */
    private Number fontSize= 18;
    /**
     *文字水平对齐方式，默认自动。
     * */
    private X align= X.center;
    /**
     *文字垂直对齐方式，默认自动。
     * */
    private Y verticalAlign;
    /**
     *行高。
     * */
    private Number lineHeight;
    /**
     *文字块的宽度。一般不用指定，不指定则自动是文字的宽度。在想做表格项或者使用图片（参见 backgroundColor）时，可能会使用它。
     * 注意，文字块的 width 和 height 指定的是内容高宽，不包含 padding。
     * width 也可以是百分比字符串，如 '100%'。表示的是所在文本块的 contentWidth（即不包含文本块的 padding）的百分之多少。
     * 之所以以 contentWidth 做基数，因为每个文本片段只能基于 content box 布局。如果以 outerWidth 做基数，则百分比的计算在实用中不具有意义，可能会超出。
     * 注意，如果不定义 rich 属性，则不能指定 width 和 height。
     * */
    private Object width;
    /**
     *文字块的高度。一般不用指定，不指定则自动是文字的高度。在使用图片（参见 backgroundColor）时，可能会使用它。
     * 注意，文字块的 width 和 height 指定的是内容高宽，不包含 padding。
     * 注意，如果不定义 rich 属性，则不能指定 width 和 height。
     * */
    private Object height;
    /**
     *文字本身的描边颜色。[ default: 'transparent' ]
     * */
    private String textBorderColor="transparent";
    /**
     *文字本身的描边宽度。[ default: 0 ]
     * */
    private Number textBorderWidth=0;
    /**
     *文字本身的阴影颜色。[ default: 'transparent' ]
     * */
    private String textShadowColor="transparent";
    /**
     *文字本身的阴影长度。[ default: 0 ]
     * */
    private Number textShadowBlur=0;
    /**
     *文字本身的阴影 X 偏移。[ default: 0 ]
     * */
    private Number textShadowOffsetX=0;
    /**
     *文字本身的阴影 Y 偏移。[ default: 0 ]
     * */
    private Number textShadowOffsetY=0;

    //TODO:之后增加rich属性


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public FontStyle getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(FontStyle fontStyle) {
        this.fontStyle = fontStyle;
    }

    public Object getFontWeight() {
        return fontWeight;
    }

    public void setFontWeight(Object fontWeight) {
        this.fontWeight = fontWeight;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public Number getFontSize() {
        return fontSize;
    }

    public void setFontSize(Number fontSize) {
        this.fontSize = fontSize;
    }

    public X getAlign() {
        return align;
    }

    public void setAlign(X align) {
        this.align = align;
    }

    public Y getVerticalAlign() {
        return verticalAlign;
    }

    public void setVerticalAlign(Y verticalAlign) {
        this.verticalAlign = verticalAlign;
    }

    public Number getLineHeight() {
        return lineHeight;
    }

    public void setLineHeight(Number lineHeight) {
        this.lineHeight = lineHeight;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public void setHeight(Number height) {
        this.height = height;
    }

    public String getTextBorderColor() {
        return textBorderColor;
    }

    public void setTextBorderColor(String textBorderColor) {
        this.textBorderColor = textBorderColor;
    }

    public Number getTextBorderWidth() {
        return textBorderWidth;
    }

    public void setTextBorderWidth(Number textBorderWidth) {
        this.textBorderWidth = textBorderWidth;
    }

    public String getTextShadowColor() {
        return textShadowColor;
    }

    public void setTextShadowColor(String textShadowColor) {
        this.textShadowColor = textShadowColor;
    }

    public Number getTextShadowBlur() {
        return textShadowBlur;
    }

    public void setTextShadowBlur(Number textShadowBlur) {
        this.textShadowBlur = textShadowBlur;
    }

    public Number getTextShadowOffsetX() {
        return textShadowOffsetX;
    }

    public void setTextShadowOffsetX(Number textShadowOffsetX) {
        this.textShadowOffsetX = textShadowOffsetX;
    }

    public Number getTextShadowOffsetY() {
        return textShadowOffsetY;
    }

    public void setTextShadowOffsetY(Number textShadowOffsetY) {
        this.textShadowOffsetY = textShadowOffsetY;
    }
}

