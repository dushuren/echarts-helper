package indi.xulala.enums;

/**
 * @Author：xulei
 * @Description：
 * @Date：2018/4/3 0003 09:10
 */
public enum Target {
    /**
     * 新窗口打开
     * */
    blank,
    /**
     * 当前窗口打开
     * */
    self;

    Target() {
    }
}
