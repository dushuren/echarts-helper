package indi.xulala.enums;

/**
 * @Author：xulei
 * @Description：
 * @Date：2018/4/3 0003 09:43
 */
public enum FontWeight {
    normal,bold,bolder,lighter
}
